import { GetCoupons } from '../application/GetCoupons';
import { Properties } from "../domain/Properties";
import {AlgorithmException} from "../domain/exceptions/AlgorithmException";

describe("Tests function", () => {
    test('Sequential number Coupons ten units', () => {
        let properties = new Properties(10, "sequentialNumbers");
        const coupons = new GetCoupons(properties).execute();
        expect(coupons[2].getCode()).toBe('000003');
        expect(coupons[6].getCode()).toBe('000007');

    });
    test('sequential number Coupons twenty units', () => {
        let properties  = new Properties(20, "sequentialNumbers");
        const coupons = new GetCoupons(properties).execute();
        expect(coupons[1].getCode()).toBe('000002');
        expect(coupons[15].getCode()).toBe('000016');

    });
    test('Pair number Coupons ten units', () => {
        let properties  = new Properties(10, "pairNumbers");
        const coupons = new GetCoupons(properties).execute();
        expect(coupons[5].getCode()).toBe('000012');
        expect(coupons[9].getCode()).toBe('000020');
    });
    test('Pair number Coupons twenty units', () => {
        let properties  = new Properties(20, "pairNumbers");
        const coupons = new GetCoupons(properties).execute();
        expect(coupons[12].getCode()).toBe('000026');
        expect(coupons[18].getCode()).toBe('000038');
    });
    test('Wrong algorithm', () => {
        let properties: Properties  = new Properties(20, "numbers");
        try {
            const coupons = new GetCoupons(properties).execute();
        } catch (e) {
            expect(typeof e).toBe(typeof new AlgorithmException());
        }
    });
});
