"use strict";
exports.__esModule = true;
var GetCoupons_1 = require("../application/GetCoupons");
var Properties_1 = require("../domain/Properties");
describe("Tests function", function () {
    test('Sequential number Coupons ten units', function () {
        var properties = new Properties_1.Properties(10, "sequentialNumbers");
        var coupons = new GetCoupons_1.GetCoupons(properties).execute();
        expect(coupons[2].getCode()).toBe('000003');
        expect(coupons[6].getCode()).toBe('000007');
    });
    test('sequential number Coupons twenty units', function () {
        var properties = new Properties_1.Properties(20, "sequentialNumbers");
        var coupons = new GetCoupons_1.GetCoupons(properties).execute();
        expect(coupons[1].getCode()).toBe('000002');
        expect(coupons[15].getCode()).toBe('000016');
    });
    test('Pair number Coupons ten units', function () {
        var properties = new Properties_1.Properties(10, "pairNumbers");
        var coupons = new GetCoupons_1.GetCoupons(properties).execute();
        expect(coupons[5].getCode()).toBe('000012');
        expect(coupons[9].getCode()).toBe('000020');
    });
    test('Pair number Coupons twenty units', function () {
        var properties = new Properties_1.Properties(20, "pairNumbers");
        var coupons = new GetCoupons_1.GetCoupons(properties).execute();
        expect(coupons[12].getCode()).toBe('000026');
        expect(coupons[18].getCode()).toBe('000038');
    });
    test('Wrong algorithm', () => {
        let properties: Properties  = new Properties(20, "numbers");
        const coupons:Coupon[] = new GetCoupons(properties).execute();
        expect(coupons).toThrowError("AlgorithmException")
    });
});
