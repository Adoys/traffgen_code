import {Coupon} from "./Coupon";
import {InvalidCodeLengthException} from "./exceptions/InvalidCodeLengthException";

export class CouponSequential extends Coupon{
    constructor(code: number) {
        super(code);
    }

    validateCode(code: number):void {
        if (code.toString().length > 6){
            throw new InvalidCodeLengthException;
        }
    }

    codeToString(code: number): string {
        return ('000000'+code).substr(-6);
    }
}
