export abstract class Coupon {
  private code: string;

  constructor(code: number) {
    this.validateCode(code)
    this.code = this.codeToString(code);
  }

  public getCode() {
    return this.code;
  }

  abstract validateCode(code: number):void

  abstract codeToString(code: number): string
}
