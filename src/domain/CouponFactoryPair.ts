import {CouponFactory} from "./CouponFactory";
import {Coupon} from "./Coupon";
import {CouponPair} from "./CouponPair";

export class CouponFactoryPair implements CouponFactory {
    constructor() {}

    generateCoupons(units:number): Coupon[] {
        let coupons: Coupon[] = [];
        let i: number = 0;

        while (coupons.length <units) {
            i = i+2;
            coupons.push(new CouponPair(i));
        }

        return coupons;
    }
}
