export class InvalidUnitFormatException extends Error {
    constructor() {
        super();
        this.name = "InvalidUnitFormatException";
    }
}
