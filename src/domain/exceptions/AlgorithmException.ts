export class AlgorithmException extends Error {
    constructor() {
        super();
        this.name = "AlgorithmException";
    }
}
