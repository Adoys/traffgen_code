export class InvalidCodeLengthException extends Error {
    constructor() {
        super();
        this.name = "InvalidCodeLengthException";
    }
}
