"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.InvalidCodeException = void 0;
var InvalidCodeException = /** @class */ (function (_super) {
    __extends(InvalidCodeException, _super);
    function InvalidCodeException() {
        var _this = _super.call(this) || this;
        _this.name = "InvalidCodeException";
        return _this;
    }
    return InvalidCodeException;
}(Error));
exports.InvalidCodeException = InvalidCodeException;
