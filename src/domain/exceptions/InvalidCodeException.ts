export class InvalidCodeException extends Error {
    constructor() {
        super();
        this.name = "InvalidCodeException";
    }
}
