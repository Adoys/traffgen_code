"use strict";
exports.__esModule = true;
exports.CouponFactorySequential = void 0;
var CouponSequential_1 = require("./CouponSequential");
var CouponFactorySequential = /** @class */ (function () {
    function CouponFactorySequential() {
    }
    CouponFactorySequential.prototype.generateCoupons = function (units) {
        var coupons = [];
        var i = 0;
        while (coupons.length < units) {
            i++;
            coupons.push(new CouponSequential_1.CouponSequential(i));
        }
        return coupons;
    };
    return CouponFactorySequential;
}());
exports.CouponFactorySequential = CouponFactorySequential;
