import {Coupon} from "./Coupon";
import {InvalidCodeLengthException} from "./exceptions/InvalidCodeLengthException";
import {InvalidCodeException} from "./exceptions/InvalidCodeException";

export class CouponPair extends Coupon{
    constructor(code: number) {
        super(code);
    }

    validateCode(code: number):void {
        if (code.toString().length > 6){
            throw new InvalidCodeLengthException;
        }
        if (code % 2 != 0) {
            throw new InvalidCodeException;
        }
    }

    codeToString(code: number): string {
        return ('000000'+code).substr(-6);
    }
}
