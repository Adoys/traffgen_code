import {CouponFactory} from "./CouponFactory";
import {CouponFactorySequential} from "./CouponFactorySequential";
import {CouponFactoryPair} from "./CouponFactoryPair";
import {AlgorithmException} from "./exceptions/AlgorithmException";

export class Algorithm {
    static factoryByAlgorithm (algorithm: string): CouponFactory {
        let returnData: CouponFactory;

        switch (algorithm) {
            case "sequentialNumbers" : {
                returnData = new CouponFactorySequential();
                break;
            }
            case "pairNumbers" : {
                returnData = new CouponFactoryPair();
                break;
            }
            default:
                throw new AlgorithmException();
        }

        return returnData;
    }
}
