"use strict";
exports.__esModule = true;
exports.Coupon = void 0;
var Coupon = /** @class */ (function () {
    function Coupon(code) {
        this.validateCode(code);
        this.code = this.codeToString(code);
    }
    Coupon.prototype.getCode = function () {
        return this.code;
    };
    return Coupon;
}());
exports.Coupon = Coupon;
