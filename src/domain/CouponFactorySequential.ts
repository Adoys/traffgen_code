import {CouponFactory} from "./CouponFactory";
import {Coupon} from "./Coupon";
import {CouponSequential} from "./CouponSequential";

export class CouponFactorySequential implements CouponFactory {
    constructor() {}

    generateCoupons(units:number): Coupon[] {
        let coupons: Coupon[] = [];
        let i: number = 0;

        while (coupons.length <units) {
            i++;
            coupons.push(new CouponSequential(i));
        }

        return coupons;
    }
}
