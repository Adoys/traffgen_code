"use strict";
exports.__esModule = true;
exports.Algorithm = void 0;
var CouponFactorySequential_1 = require("./CouponFactorySequential");
var CouponFactoryPair_1 = require("./CouponFactoryPair");
var AlgorithmException_1 = require("./exceptions/AlgorithmException");
var Algorithm = /** @class */ (function () {
    function Algorithm() {
    }
    Algorithm.factoryByAlgorithm = function (algorithm) {
        var returnData;
        switch (algorithm) {
            case "sequentialNumbers": {
                returnData = new CouponFactorySequential_1.CouponFactorySequential();
                break;
            }
            case "pairNumbers": {
                returnData = new CouponFactoryPair_1.CouponFactoryPair();
                break;
            }
            default:
                throw new AlgorithmException_1.AlgorithmException();
        }
        return returnData;
    };
    return Algorithm;
}());
exports.Algorithm = Algorithm;
