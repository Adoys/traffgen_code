import {Coupon} from "./Coupon";

export interface CouponFactory {
    generateCoupons(units:number): Coupon[];
}
