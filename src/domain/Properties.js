"use strict";
exports.__esModule = true;
exports.Properties = void 0;
var Properties = /** @class */ (function () {
    function Properties(units, algorithm) {
        this.units = units;
        this.algorithm = algorithm;
    }
    Properties.prototype.getUnits = function () {
        return this.units;
    };
    Properties.prototype.getAlgorithm = function () {
        return this.algorithm;
    };
    return Properties;
}());
exports.Properties = Properties;
