export class Properties  {
    constructor(private units: number, private algorithm: string) {}

    getUnits(): number {
        return this.units;
    }

    getAlgorithm():string {
        return this.algorithm
    }
}

