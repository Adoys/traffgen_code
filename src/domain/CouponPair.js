"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.CouponPair = void 0;
var Coupon_1 = require("./Coupon");
var InvalidCodeLengthException_1 = require("./exceptions/InvalidCodeLengthException");
var InvalidCodeException_1 = require("./exceptions/InvalidCodeException");
var CouponPair = /** @class */ (function (_super) {
    __extends(CouponPair, _super);
    function CouponPair(code) {
        return _super.call(this, code) || this;
    }
    CouponPair.prototype.validateCode = function (code) {
        if (code.toString().length > 6) {
            throw new InvalidCodeLengthException_1.InvalidCodeLengthException;
        }
        if (code % 2 != 0) {
            throw new InvalidCodeException_1.InvalidCodeException;
        }
    };
    CouponPair.prototype.codeToString = function (code) {
        return ('000000' + code).substr(-6);
    };
    return CouponPair;
}(Coupon_1.Coupon));
exports.CouponPair = CouponPair;
