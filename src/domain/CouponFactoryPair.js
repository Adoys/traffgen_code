"use strict";
exports.__esModule = true;
exports.CouponFactoryPair = void 0;
var CouponPair_1 = require("./CouponPair");
var CouponFactoryPair = /** @class */ (function () {
    function CouponFactoryPair() {
    }
    CouponFactoryPair.prototype.generateCoupons = function (units) {
        var coupons = [];
        var i = 0;
        while (coupons.length < units) {
            i = i + 2;
            coupons.push(new CouponPair_1.CouponPair(i));
        }
        return coupons;
    };
    return CouponFactoryPair;
}());
exports.CouponFactoryPair = CouponFactoryPair;
