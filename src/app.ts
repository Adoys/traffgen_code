import {GetPropertiesFromJSON} from "./infraestructure/dataSource/GetPropertiesFromJSON";
import {Properties} from "./domain/Properties";
import {GetCoupons} from "./application/GetCoupons";
import {Coupon} from "./domain/Coupon";
import {ConsolePrintCoupon} from "./infraestructure/presenter/ConsolePrintCoupon";

function main () {
    let properties: Properties = new GetPropertiesFromJSON().execute();
    let coupons: Coupon[] = new GetCoupons(properties).execute();
    new ConsolePrintCoupon().execute(coupons);
}

try {
    main()
} catch (e) {
    console.log(e);
}
