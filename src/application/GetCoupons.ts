import {Properties} from "../domain/Properties";
import {Coupon} from "../domain/Coupon";
import {Algorithm} from "../domain/Algorithm";

export class GetCoupons {
    constructor(private properties: Properties){}

    execute(): Coupon[] {
        return Algorithm.factoryByAlgorithm(this.properties.getAlgorithm()).generateCoupons(this.properties.getUnits());
    }
}
