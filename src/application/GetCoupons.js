"use strict";
exports.__esModule = true;
exports.GetCoupons = void 0;
var Algorithm_1 = require("../domain/Algorithm");
var GetCoupons = /** @class */ (function () {
    function GetCoupons(properties) {
        this.properties = properties;
    }
    GetCoupons.prototype.execute = function () {
        return Algorithm_1.Algorithm.factoryByAlgorithm(this.properties.getAlgorithm()).generateCoupons(this.properties.getUnits());
    };
    return GetCoupons;
}());
exports.GetCoupons = GetCoupons;
