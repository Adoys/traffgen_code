import {Properties} from "../../domain/Properties";
import {GetProperties} from "./GetProperties";
import {InvalidUnitFormatException} from "../../domain/exceptions/InvalidUnitFormatException";
const fileJSON = require("../../properties.json");

export class GetPropertiesFromJSON implements GetProperties {
    constructor() {
    }

    execute(): Properties {
        let units = parseInt(fileJSON.coupons.units);
        if (isNaN(units)) { throw new InvalidUnitFormatException(); }
        return new Properties(units, fileJSON.coupons.algorithm);
    }
}
