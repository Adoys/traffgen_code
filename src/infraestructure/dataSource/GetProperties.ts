import {Properties} from "../../domain/Properties";

export interface GetProperties {
    execute():Properties;
}
