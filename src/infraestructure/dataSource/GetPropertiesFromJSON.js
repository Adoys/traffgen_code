"use strict";
exports.__esModule = true;
exports.GetPropertiesFromJSON = void 0;
var Properties_1 = require("../../domain/Properties");
var InvalidUnitFormatException_1 = require("../../domain/exceptions/InvalidUnitFormatException");
var fileJSON = require("../../properties.json");
var GetPropertiesFromJSON = /** @class */ (function () {
    function GetPropertiesFromJSON() {
    }
    GetPropertiesFromJSON.prototype.execute = function () {
        var units = parseInt(fileJSON.coupons.units);
        if (isNaN(units)) {
            throw new InvalidUnitFormatException_1.InvalidUnitFormatException();
        }
        return new Properties_1.Properties(units, fileJSON.coupons.algorithm);
    };
    return GetPropertiesFromJSON;
}());
exports.GetPropertiesFromJSON = GetPropertiesFromJSON;
