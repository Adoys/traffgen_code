import {Coupon} from "../../domain/Coupon";

export interface PrintCoupon {
    execute (coupons: Coupon[]): void
}
