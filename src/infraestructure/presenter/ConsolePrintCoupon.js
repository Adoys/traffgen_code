"use strict";
exports.__esModule = true;
exports.ConsolePrintCoupon = void 0;
var ConsolePrintCoupon = /** @class */ (function () {
    function ConsolePrintCoupon() {
    }
    ConsolePrintCoupon.prototype.execute = function (coupons) {
        coupons.forEach(function (value) {
            console.log(value.getCode());
        });
    };
    return ConsolePrintCoupon;
}());
exports.ConsolePrintCoupon = ConsolePrintCoupon;
