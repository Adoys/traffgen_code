import {PrintCoupon} from "./PrintCoupon";
import {Coupon} from "../../domain/Coupon";

export class ConsolePrintCoupon implements PrintCoupon {
    execute (coupons: Coupon[]): void {
        coupons.forEach(function (value){
            console.log(value.getCode());
        });
    }
}
