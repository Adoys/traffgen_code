"use strict";
exports.__esModule = true;
var GetPropertiesFromJSON_1 = require("./infraestructure/dataSource/GetPropertiesFromJSON");
var GetCoupons_1 = require("./application/GetCoupons");
var ConsolePrintCoupon_1 = require("./infraestructure/presenter/ConsolePrintCoupon");
function main() {
    var properties = new GetPropertiesFromJSON_1.GetPropertiesFromJSON().execute();
    var coupons = new GetCoupons_1.GetCoupons(properties).execute();
    new ConsolePrintCoupon_1.ConsolePrintCoupon().execute(coupons);
}
try {
    main();
}
catch (e) {
    console.log(e);
}
